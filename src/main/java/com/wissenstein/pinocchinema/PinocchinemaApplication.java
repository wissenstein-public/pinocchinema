package com.wissenstein.pinocchinema;

import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PinocchinemaApplication {

    private static final Logger logger = LoggerFactory.getLogger(PinocchinemaApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(PinocchinemaApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(final ApplicationContext ctx) {
        return args -> {
            logger.info(">>>> Pinocchinema demo application");

            Stream.of(ctx.getBeanDefinitionNames())
                    .sorted()
                    .forEach(beanName -> logger.info(beanName));

            logger.info(">>>> That's it!");
        };
    }
}
